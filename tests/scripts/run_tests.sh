#!/bin/bash
set -eux

if [ ! -e /usr/share/lorax/templates.d/80-rhel/ ]; then
    echo "Failed to find lorax-templates-rhel templates in /usr/share/lorax/templates.d/"
    exit 1
fi

# NOTE: This must be able to run both on the Zuul using centos repos and on osci gating using rhel
# so filter for the list of allowed repos
REPODIR="/etc/yum.repos.d"
REPOFILES="rhel.repo centos.repo test-artifacts.repo"
REPOS=""
for r in $REPOFILES; do
    [ -e "$REPODIR/$r" ] && REPOS+="--repo $REPODIR/$r "
done
if [ -z "$REPOS" ]; then
    echo "No repos ($REPOFILES) found in $REPODIR"
    exit 1
fi

# The c10s tmt systems have started using $stream in the repo urls as of 6/2024, substitute
# 10-stream for this variable so that they can be passed to lorax
sed -i 's/$stream/10-stream/g' /etc/yum.repos.d/*.repo

# Run lorax using the host's repository configuration file
lorax --product="Red Hat Enterprise Linux" --version=10 --release=10 --volid="RHEL-10-test" \
      $REPOS --isfinal --nomacboot /var/tmp/lorax-rhel10-iso/
